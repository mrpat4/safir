import globalUtils from "../../../globalUtils";
import panelAdmin from "../..";

const owners = async ({ page }) => {
  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.OWNERS;
  return axios.get(strings + "/" + page);
  // .then((owners) => {
  //   console.log({ owners });
  //   returnData(owners.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default owners;
