import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
const FormInputGenres = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;

  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress;

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");

        changed = (e) =>
          inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputGenres;
