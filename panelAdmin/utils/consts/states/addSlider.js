const addSlider = {
  Form: {
    parent: {
      label: "فروشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    // type: {
    //   label: "ت :",

    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addSlider;
