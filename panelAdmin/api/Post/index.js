import imageUpload from "./imageUpload";
import category from "./category";
import product from "./product";
import owner from "./owner";
import store from "./store";
import slider from "./slider";
import banner from "./banner";
import notification from "./notification";
import version from "./version";

const post = {
  imageUpload,
  category,
  product,
  owner,
  store,
  slider,
  banner,
  notification,
  version,
};
export default post;
