import atRedux from "../actionTypes/redux";

export const homeInitialState = {
    homeData: [],
};


export const setHomeData = (state, action) => {
    return {...state, homeData: action.data}
};


function homeReducer(state = homeInitialState, action) {
    switch (action.type) {
        case atRedux.SET_HOME_DATA:
            return setHomeData(state, action);
        default:
            return state
    }
}

export default homeReducer;