import React from "react";
import LazyImage from "../../../../components/LazyImage";
import PropTypes from "prop-types";
const GalleryImageCard = ({ image, title, handelAcceptedImage, imageAccepted }) => {
  let imageActive = null;
  if (imageAccepted)
    if (imageAccepted.web || imageAccepted.phone)
      imageActive = imageAccepted.web === image.web || imageAccepted.phone === image.phone ? "activeImage" : "";

  return (
    <li
      onClick={() => (imageAccepted ? handelAcceptedImage(image) : null)}
      className="gallery-image-card  px-1 col-6 col-md-5 col-xl-2 col-lg-2"
    >
      <div className={`gallery-image-card-container ${imageActive}`}>
        <picture>
          <source media="(max-width: 375px)" srcSet={image.phone} />

          {/* <img id="myImage" className="noSelect noEvent" src={images.web} alt={titleTop} /> */}
          <LazyImage src={image.web} defaultImage={false} alt={title} />
        </picture>
        {/* <img src={image} alt={title} /> */}
        <div className="card-image-text-data">
          <p>{title}</p>
        </div>
      </div>
    </li>
  );
};
GalleryImageCard.propTypes = {
  image: PropTypes.object,
  title: PropTypes.string,
  imageAccepted: PropTypes.object,
  handelAcceptedImage: PropTypes.func,
};
export default GalleryImageCard;
