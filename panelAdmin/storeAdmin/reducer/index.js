// import galleryReducer from "./galleryReducer";
import uploadReducer from "./uploadReducer";
import galleryReducer from "./galleryReducer";
import categoryReducer from "./categoryReducer";
import panelNavbarReducer from "./panelNavbarReducer";
import loadingReducer from "./loadingReducer";
import productReducer from "./productReducer";
const reducer = {
  upload: uploadReducer,
  gallery: galleryReducer,
  category: categoryReducer,
  panelNavbar: panelNavbarReducer,
  loading: loadingReducer,
  product: productReducer,
};

export default reducer;
