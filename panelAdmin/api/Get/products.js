import axios from "../../../globalUtils/axiosBase";
import panelAdmin from "../..";

const products = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.PRODUCT;
  return axios.get(strings + "/" + page);
  // .then((products) => {
  //   console.log({ products });
  //   returnData(products.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default products;
