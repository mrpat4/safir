import App from "next/app";
import dynamic from "next/dynamic";
import Head from "next/head";
import "../public/styles/index.scss";
import PanelScreen from "../panelAdmin/screen/PanelScreen";
import WebsiteScreen from "../website/screen/WebsiteScreen";
import ErrorHandler from "../website/baseComponents/ErrorHandler";
import { SWRConfig } from "swr";
import axios from "../globalUtils/axiosBase";
// const PanelScreen = dynamic(() => import("../panelAdmin/screen/PanelScreen"), { ssr: false });
class safirApp extends App {
  // static async getInitialProps({ Component, ctx }) {
  //   let pageProps = {};
  //   if (Component.getInitialProps) {
  //     pageProps = await Component.getInitialProps({ ctx });
  //   }

  //   return { pageProps };
  // }
  render() {
    const { Component, pageProps, store, router } = this.props;
    let body;
    if (router.asPath.includes("/panelAdmin")) {
      body = (
        <PanelScreen router={router}>
          <Component {...pageProps} />
        </PanelScreen>
      );
    }
    //  else {
    //   body = (
    //     <WebsiteScreen>
    //       <Component {...pageProps} />
    //     </WebsiteScreen>
    //   );
    // }
    return (
      <div>
        <Head>
          <title>{"سفیر "}</title>
          <meta name="description" content="فروشگاه اینترنتی    ." />
          <meta name="keywords" content=" ,موسیقی و ,IPTV,موسیقی,پرداخت اجتماعی،پخش زنده،اپلیکیشن,سرگرمی," />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <link href={"/styles/css/styles.css"} rel={"stylesheet"} />
        </Head>

        <div className="base-page">
          <SWRConfig value={{ refreshInterval: 0, fetcher: (...args) => axios(...args).then((r) => r.data) }}>
            {body} <ErrorHandler />
          </SWRConfig>
        </div>
      </div>
    );
  }
}

export default safirApp;
