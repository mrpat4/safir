const TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = { TRACKS, NO_ENTRIES: NO_ENTRIES, FOLLOWERS };
export default global;
