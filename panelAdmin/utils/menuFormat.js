import panelAdmin from "..";
import values from "../values/index";

const menuFormat = [
  {
    title: "عمومی",
    menus: [
      {
        route: values.routes.GS_ADMIN_DASHBOARD,
        menuTitle: values.strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: values.strings.GALLERY,
        menuIconImg: false,
        menuIconClass: "fad fa-images",
        subMenu: [
          {
            title: values.strings.SEE_GALLERIES,
            route: values.routes.GS_ADMIN_GALLERY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.CATEGORY,
        menuIconImg: false,
        menuIconClass: "fad fa-cubes",
        subMenu: [
          {
            title: values.strings.SEE_CATEGORIES,
            route: values.routes.GS_ADMIN_CATEGORY,
          },
          {
            title: values.strings.ADD_CATEGORY,
            route: values.routes.GS_ADMIN_ADD_CATEGORY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.OWNER,
        menuIconImg: false,
        menuIconClass: "far fa-street-view",
        subMenu: [
          {
            title: values.strings.SEE_OWNERS,
            route: values.routes.GS_ADMIN_OWNER,
          },
          {
            title: values.strings.ADD_OWNER,
            route: values.routes.GS_ADMIN_ADD_OWNER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.STORE,
        menuIconImg: false,
        menuIconClass: "fad fa-store",
        subMenu: [
          {
            title: values.strings.SEE_STORES,
            route: values.routes.GS_ADMIN_STORE,
          },
          {
            title: values.strings.ADD_STORE,
            route: values.routes.GS_ADMIN_ADD_STORE,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.SLIDER,
        menuIconImg: false,
        menuIconClass: "fas fa-presentation",
        subMenu: [
          {
            title: values.strings.SEE_SLIDERS,
            route: values.routes.GS_ADMIN_SLIDER,
          },
          {
            title: values.strings.ADD_SLIDER,
            route: values.routes.GS_ADMIN_ADD_SLIDER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.BANNER,
        menuIconImg: false,
        menuIconClass: "far fa-archive",
        subMenu: [
          {
            title: values.strings.SEE_BANNERS,
            route: values.routes.GS_ADMIN_BANNER,
          },
          {
            title: values.strings.ADD_BANNER,
            route: values.routes.GS_ADMIN_ADD_BANNER,
          },
        ],
      },
      {
        route: values.routes.GS_ADMIN_ADD_NOTIFICATION,
        menuTitle: values.strings.NOTIFICATION,
        menuIconImg: false,
        menuIconClass: "fad fa-bells",
        subMenu: [
          // {
          //   title: values.strings.SEE_NOTIFICATIONS,
          //   route: values.routes.GS_ADMIN_NOTIFICATION,
          // },
          // {
          //   title: values.strings.ADD_NOTIFICATION,
          //   route: values.routes.GS_ADMIN_ADD_NOTIFICATION,
          // },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.VERSION,
        menuIconImg: false,
        menuIconClass: "fab fa-vimeo-v",
        subMenu: [
          {
            title: values.strings.SEE_VERSIONS,
            route: values.routes.GS_ADMIN_VERSION,
          },
          {
            title: values.strings.ADD_VERSION,
            route: values.routes.GS_ADMIN_ADD_VERSION,
          },
        ],
      },
    ],
  },
];

export default menuFormat;
