import React, { useEffect, useState, Fragment, useRef } from "react";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
import utils from "../../../utils";
import PaginationM from "../../../component/UI/PaginationM";

const BannerScreen = (props) => {
  const { onDataChange, filters, acceptedCardInfo, requestData, handelPage } = props;
  console.log({ requestData });

  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });
  const inputRef = useRef(null);

  const card = panelAdmin.utils.consts.card;

  const showDataElement = <ShowCardInformation data={card.banner(requestData?.docs)} onClick={null} optionClick={null} />;
  // const showDataElement = <Table tableStructure={utils.json.table.slider} data={requestData} />;
  return (
    <React.Fragment>
      <div style={{ flex: "1" }} className="flexColum">
        {showDataElement}

        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={handelPage} />
      </div>{" "}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
    </React.Fragment>
  );
};

export default BannerScreen;
