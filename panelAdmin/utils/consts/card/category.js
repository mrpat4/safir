import React from "react";

const category = (data) => {
  const cardFormat = [];
  console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let titleFa = dataIndex.titleFa ? dataIndex.titleFa : "";
    let titleEn = dataIndex.titleEn ? dataIndex.titleEn : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: titleFa, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          left: [{ elementType: "text", value: titleEn, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }

  return cardFormat;
};

export default category;
