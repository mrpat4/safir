import table from "./table";
import states from "./states";
import galleryConstants from "./galleryConstants";
import card from "./card";
import modal from "./modal";

const consts = { table, states, galleryConstants, card, modal };
export default consts;
