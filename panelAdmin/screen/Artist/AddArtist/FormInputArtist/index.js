import React, { useState, useEffect } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import { get } from "../../../../api";
import { useSelector, useDispatch } from "react-redux";
const FormInputArtist = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;

  const [Genres, setGenres] = useState();
  const [BirthPlaceCountry, setBirthPlaceCountry] = useState();
  const [Instrument, setInstrument] = useState();

  const accept = (event) => inputChangedHandler(event);
  useEffect(() => {
    get.genres({ page: false }).then((res) => {
      setGenres(res.data);
    });
    get.country({ page: false }).then((res) => {
      setBirthPlaceCountry(res.data);
    });
    get.instrument({ page: false }).then((res) => {
      setInstrument(res.data);
    });
  }, []);
  // =========================================================================== SEARCH STRUCTURE FOR DROP DOWN
  // =========================== FOR DROP DOWN GENRE ===========================
  let GenreData = [];
  if (Genres)
    for (const index in Genres)
      GenreData.push({
        value: Genres[index]._id,
        title: Genres[index].titleFa,
        description: Genres[index].titleEn,
      });
  // =========================== FOR BIRTH PLACE COUNTRY ===========================
  let BirthPlaceCountryData = [];
  if (BirthPlaceCountry)
    for (const index in BirthPlaceCountry)
      BirthPlaceCountryData.push({
        value: BirthPlaceCountry[index]._id,
        title: BirthPlaceCountry[index].titleFa,
        description: BirthPlaceCountry[index].titleEn,
      });
  // =========================== FOR INSTRUMENT PLACE COUNTRY ===========================
  let InstrumentData = [];
  if (Instrument)
    for (const index in Instrument)
      InstrumentData.push({
        value: Instrument[index]._id,
        title: Instrument[index].titleFa,
        description: Instrument[index].titleEn,
      });
  // =========================================================================== FORM
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement, index) => {
        // ================================= VARIABLE VALUES
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, dropDownData, disabled;
        disabled = false;
        let value = formElement.config.value;
        const inputClasses = ["InputElement"];
        // ================================= CHANGE ELEMENT AMOUNTS
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "avatar") {
          value = formElement.config.value.web ? formElement.config.value.web : "";
          disabled = true;
          accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
        } else if (formElement.id === "birthday") {
          changed = (e, child) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, child });
        } else if (formElement.id === "genre") {
          dropDownData = GenreData;
          accepted = (value) => accept({ value, name: formElement.id });
        } else if (formElement.id === "defaultGenre") {
          dropDownData = GenreData;
          accepted = (value) => accept({ value, name: formElement.id });
        } else if (formElement.id === "birthPlaceCountry") {
          // changed = BirthPlaceCountry;
          dropDownData = BirthPlaceCountryData;
          accepted = (value) => accept({ value, name: formElement.id });
        } else if (formElement.id === "instruments") {
          dropDownData = InstrumentData;
          accepted = (value) => accept({ value, name: formElement.id });
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }
        // ================================= INPUTS
        let Form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
            disabled={disabled}
          />
        );

        return Form;
      })}
    </form>
  );
};

export default FormInputArtist;
