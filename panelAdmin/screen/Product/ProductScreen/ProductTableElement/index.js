import React from "react";
import Pagination from "../../../../component/UI/PaginationM";
import InputVsIcon from "../../../../component/UI/InputVsIcon";
import panelAdmin from "../../../..";
// import IsNull from "../../../../components/UI/IsNull";
import TabelBasic from "../../../../component/UI/Tables";
const ProductTableElement = (props) => {
  const { ApiData, handelPage, tableOnclick, handelChange } = props;
  return (
    <div className="countainer-main  ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <TabelBasic
          subTitle={
            <div className="disColum">
              <h4
                style={{
                  color: "black",
                  paddingBottom: "0.2em",
                  fontSize: "1.5em",
                }}
              >
                {" محصولات "}
              </h4>
              {ApiData ? <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${ApiData.total}  `}</span> : " "}
            </div>
          }
          imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
          tbody={panelAdmin.utils.consts.table.products(ApiData ? ApiData.docs : "").tbody}
          thead={panelAdmin.utils.consts.table.products(ApiData ? ApiData.docs : "").thead}
          onClick={tableOnclick}
          inputRef
          searchHead={InputVsIcon({
            name: "genreTitle",
            icon: "far fa-search",
            placeholder: "جستجو...",
            onChange: handelChange,
            dir: "ltr",
          })}
        />

        {ApiData && ApiData.pages >= 2 && <Pagination limited={"3"} pages={ApiData ? ApiData.pages : ""} activePage={ApiData ? ApiData.page : ""} onClick={handelPage} />}
      </div>
    </div>
  );
};

export default ProductTableElement;
