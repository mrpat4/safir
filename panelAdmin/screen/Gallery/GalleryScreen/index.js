import React, { useEffect, useState, Fragment, useRef } from "react";
import AddGallery from "../AddGallery";
import { Modal, Button } from "react-bootstrap";
import panelAdmin from "../../..";
import _ from "lodash";
import MyVerticallyCenteredModal from "../../../component/UI/Modals/MyVerticallyCenteredModal";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import useApiRequest from "../../../../lib/useApiRequest";
import useSWR, { mutate, trigger, useSWRPages } from "swr";
import globalUtils from "../../../../globalUtils";
import lib from "../../../../lib";
import Table from "../../../component/UI/Tables/Table";
import PaginationM from "../../../component/UI/PaginationM";
import utils from "../../../utils";
const axios = globalUtils.axiosBase;

const GalleryScreen = (props) => {
  const { apiPageFetch, filters, acceptedCardInfo, requestData, loadingApi } = props;

  // ========================================  STATE
  const states = panelAdmin.utils.consts.states;
  const [showUploadModal, setUploadModal] = useState(false);
  const [data, setData] = useState({ ...states.addGallery });
  const [infiniteScrollEnabled, setInfiniteScrollEnabled] = useState(false);
  const $loadMoreButton = useRef(null);
  const infiniteScrollCount = useRef(1);
  // const isOnScreen = lib.useOnScreen($loadMoreButton, "200px");
  // console.log({ isOnScreen });

  // ========================================  CONST
  // const CurrentPage = info?.page || "1";
  const strings = panelAdmin.values.apiString;
  // const URL = strings.IMAGE + "/" + CurrentPage;
  const card = panelAdmin.utils.consts.card;

  // ========================================  SWR config

  // const { data: requestData } = useApiRequest(
  //   URL,
  //   { initialData: info },
  //   {
  //     refreshInterval: 0,
  //   }
  // );
  const count = infiniteScrollCount.current;
  // useEffect(() => {
  //   console.log("one");

  //   if (!infiniteScrollEnabled || !isOnScreen) return;
  //   console.log("two");

  //   console.log({ count: infiniteScrollCount });

  //   // if (count + 1 === 3) {
  //   //   setInfiniteScrollEnabled(false);
  //   //   infiniteScrollCount.current = 0;
  //   // } else {
  //   infiniteScrollCount.current = count + 1;
  //   // }
  // }, [infiniteScrollEnabled, isOnScreen]);
  // ========================================  SUBMIT => ADD GALLERY

  const _onSubmit = async (e) => {
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    trigger(URL);
    setData({ ...states.addGallery });
  };

  // ========================================  MODAL CONFIG
  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: "ثبت",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit,
  };

  // ========================================  CARD ELEMENT
  const showDataElement = <ShowCardInformation data={card.gallery(requestData?.docs, acceptedCardInfo?.acceptedCard)} onClick={null} optionClick={null} acceptedCardInfo={acceptedCardInfo} />;

  // // ========================================  CHECK HAS DATA
  // if (error) return <div>failed to load</div>;
  // if (!requestData) return <div>loading...</div>;
  // <button style={{ width: "50px", height: "50px" }} onClick={() => setCurrentPage(currentPage + 1)} />;
  // ========================================  RETURN

  // const showDataElement = <Table tableStructure={utils.json.table.gallery} data={requestData} />;
  const handelPage = (number) => {
    apiPageFetch(number);
  };

  return (
    <div className="gallery">
      <MyVerticallyCenteredModal {...modalData}>
        <AddGallery data={data} setData={setData} />
      </MyVerticallyCenteredModal>
      <div className="gallery-header-wrapper">
        <div className="gallery-header">
          <Button onClick={() => setUploadModal(true)} className="">
            {"افزودن عکس"}
          </Button>
        </div>
        {showDataElement}
        {/* {pages} */}
        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={handelPage} />
      </div>
    </div>
  );
};
export default GalleryScreen;
