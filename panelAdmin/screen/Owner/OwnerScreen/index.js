import React, { useState } from "react";
import panelAdmin from "../../..";
import OwnerTableElement from "./OwnerTableElement";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import AddOwner from "../AddOwner";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
const OwnerScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData: ApiData } = props;
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    if (await panelAdmin.api.deletes.product(id)) {
      apiPageFetch(ApiData.page);
      onHideModal();
    }
  };
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal

  // const renderModalInputs = (
  //   <div className={ModalInpts.kindOf === "edit" ? "width80" : ""}>
  //     <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
  //       {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevStep} />}
  //       {ModalInpts.kindOf === "edit" && <AddOwner editData={ModalInpts.editData}  modalAccept={__returnPrevStep} />}
  //     </ModalBox>
  //   </div>
  // );
  // ========================================END modal

  const tableOnclick = (index, kindOf) => {
    console.log({ index, kindOf });

    switch (kindOf) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: ApiData.docs[index]._id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: ApiData.docs[index] });
        break;
      default:
        break;
    }
  };
  return (
    <React.Fragment>
      {/* {renderModalInputs} */}
      <ModalStructure modalRequest={modalRequest} reqApiRemove={reqApiRemove} onHideModal={onHideModal} modalDetails={modalDetails}>
        {modalDetails.kindOf === "component" && <AddOwner propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <OwnerTableElement requestData={ApiData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={null} />
    </React.Fragment>
  );
};

export default OwnerScreen;
