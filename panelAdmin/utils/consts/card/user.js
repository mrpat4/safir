import React from "react";
import PanelString from "../../../value/PanelString";
// import userPng from "../../../assets/Images/icons/user.png";
const user = (data) => {
  let NotSelected = "انتخاب نشده";
  const cardFormat = [];
  for (let index in data) {
    let fullName = data[index].fullName ? data[index].fullName : NotSelected;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotSelected;
    let address = data[index].address ? data[index].address : NotSelected;
    let job = data[index].job ? data[index].job : NotSelected;
    let basketLength = data[index].courses.length + data[index].seminars.length + data[index].audioBooks.length;
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: data[index].avatar ? data[index].avatar : userPng },
      body: [
        {
          right: [{ elementType: "text", value: fullName, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
          left: [{ elementType: "text", value: job, style: { color: PanelString.color.GREEN, fontSize: "1em", fontWeight: "800" } }],
        },
        {
          right: [{ elementType: "text", value: address, title: address, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
        },
        {
          right: [{ elementType: "icon", value: basketLength, className: "icon-basket", direction: "ltr", style: { fontSize: "1.4em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
          left: [{ elementType: "icon", value: phoneNumber, className: "icon-phone", direction: "ltr", style: { fontSize: "1em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default user;
