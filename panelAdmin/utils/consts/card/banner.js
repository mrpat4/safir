const banner = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let NO_ENTERED = "وارد نشده";
    let type = dataIndex.type ? dataIndex.type : NO_ENTERED;
    let store = dataIndex.store ? dataIndex.store : false;
    let title = store.title ? store.title : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED;
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard === image ? "activeImage" : "") : "",
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default banner;
