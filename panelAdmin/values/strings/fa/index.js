import navbar from "./navbar.js";
import sideMenu from "./sideMenu.js";
import global from "./global.js";
import constants from "./constants";

const fa = {
  ...navbar,
  ...sideMenu,
  ...global,
  ...constants,
};
export default fa;
