import strings from "./strings";
import routes from "./routes";
import apiString from "./apiString";
import constants from "./strings/constants";
import fa from "./strings/fa";
import en from "./strings/en";
const values = { routes, strings, apiString, constants, fa, en };
export default values;
