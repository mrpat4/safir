import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
const FormInputNotification = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;

  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress, disabled;
        let value = formElement.config.value;
        disabled = false;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "image") {
          // progress = state.progressPercentImage;
          value = formElement.config.value ? formElement.config.value : "";
          disabled = true;
          accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
        } else {
          changed = (e) =>
            inputChangedHandler({
              value: e.currentTarget.value,
              name: formElement.id,
              type: e.currentTarget.type,
              files: e.currentTarget.files,
            });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
            disabled={disabled}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputNotification;
