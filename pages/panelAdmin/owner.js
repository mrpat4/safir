import React, { useEffect, useContext, useState } from "react";
import OwnerScreen from "../../panelAdmin/screen/Owner/OwnerScreen";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const owner = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";

  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });
  useEffect(() => {
    dispatch.changePageName("فروشنده");
    apiPageFetch("1");
  }, []);
  // console.log({ data, resData });

  const apiPageFetch = async (page = 1) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.owners({ page });
    console.log({ res });
    setState(res.data);
    setLoadingApi(false);
  };

  return (
    <>
      <OwnerScreen requestData={state} acceptedCardInfo={acceptedCardInfo} apiPageFetch={apiPageFetch} />;{/* {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )} */}
    </>
  );
  // return true;
};
// ========================================= getInitialProps
// owner.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.owners({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };

export default owner;
