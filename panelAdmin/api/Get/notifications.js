import axios from "../../../globalUtils/axiosBase";
import panelAdmin from "../..";

const notifications = async (page) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  let getUrl = page ? strings.NOTIFICATION + "/" + page : strings.NOTIFICATION;

  return axios.get(getUrl);
  // .then((res) => {
  //   console.log({ notifications: res });

  //   return res;
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  //   return error;
  // });
};

export default notifications;
