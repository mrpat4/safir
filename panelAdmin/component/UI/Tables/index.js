import React, { Fragment, useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import Star from "../Rating/Star";
import Pageination from "../PaginationM";
import IsNull from "../IsNull";
const TableBasic = (props) => {
  const { thead, tbody, onClick, imgStyle, subTitle, btnHead, pageination, searchHead, onChange } = props;
  const [pageinat, setpageinat] = useState(false);
  useEffect(() => {
    if (pageination)
      setpageinat({
        total: tbody.length,
        limit: pageination.limited,
        page: "1",
        pages: Math.ceil(tbody.length / pageination.limited),
      });
    else setpageinat(false);
  }, []);
  let body = [];
  if (pageination) body = tbody.slice(pageinat.limit * pageinat.page - pageinat.limit, pageinat.limit * pageinat.page);
  else body = tbody;

  const _handelPage = (value) => {
    if (value) setpageinat({ ...pageinat, page: value });
  };
  const optionElement = (option, parentIndex) => {
    return (
      <Fragment>
        {option.edit && (
          <span>
            <i className="far fa-edit" onClick={() => onClick(parentIndex, "edit")} />
          </span>
        )}
        {option.remove && (
          <span>
            <i className="fas fa-trash-alt" onClick={() => onClick(parentIndex, "remove")} />
          </span>
        )}
        {option.eye && (
          <span>
            <i className="fas fa-eye" onClick={() => onClick(parentIndex, "showModal", option.name)} />
          </span>
        )}
        {option.star && (
          <span>
            <Star rating={option.value} fixed size={"small"} />
          </span>
        )}
        {option.play && (
          <span>
            <i onClick={() => onClick(parentIndex, "play")} class="fal fa-play"></i>
          </span>
        )}
      </Fragment>
    );
  };
  return (
    <Fragment>
      {subTitle && (
        <div className="subtitle-elements">
          <span className="centerAll">{subTitle} </span>
          {btnHead ? (
            <div className="btns-container">
              <button onClick={btnHead.onClick} className="btns btns-primary">
                {btnHead.title}
              </button>
            </div>
          ) : searchHead ? (
            searchHead
          ) : (
            ""
          )}
        </div>
      )}

      {body && body.length ? (
        <div className="show-elements">
          <Table striped bordered hover size="sm" className="table-wrapper">
            <thead>
              <tr>
                {thead &&
                  thead.map((infoHead, parentIndex) => {
                    return (
                      <th className="textCenter" key={parentIndex + "mor"}>
                        {infoHead}
                      </th>
                    );
                  })}
              </tr>
            </thead>

            <tbody>
              {body &&
                body.length &&
                body.map((infoBody, parentIndex) => {
                  return (
                    <tr style={infoBody.style} key={parentIndex + "mojiii"}>
                      {pageination ? <td className="textCenter">{pageinat.limit * pageinat.page - pageinat.limit + parentIndex + 1}</td> : <td className="textCenter">{parentIndex + 1}</td>}
                      {infoBody.data.map((info, childIndex) => {
                        const pattern = RegExp("http");
                        return info ? (
                          pattern.test(info) ? (
                            <td key={childIndex + "mor"} className="textCenter">
                              <img style={imgStyle} src={info} alt="image" />
                            </td>
                          ) : info.option ? (
                            <td className=" table-wrapper " key={childIndex + "mo"}>
                              {optionElement(info.option, parentIndex)}
                            </td>
                          ) : (
                            <td key={childIndex + "mojta"} className="textCenter">
                              {info}
                            </td>
                          )
                        ) : (
                          ""
                        );
                      })}
                    </tr>
                  );
                })}
            </tbody>
          </Table>
          <div className="centerAll width100">{pageination && pageinat.pages > 1 && <Pageination limited={"2"} pages={pageinat.pages} activePage={pageinat.page} onClick={_handelPage} />}</div>{" "}
        </div>
      ) : (
        ""
        // <IsNull title={"موردی یافت نشد"} />
      )}
    </Fragment>
  );
};

export default TableBasic;
