import axios from "../../../utils/axiosBase";
import panelAdmin from "../..";

const artistSearch = async ({ title, page }) => {
  console.log({ cattitle: title });
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;

  return axios.get(strings.ARTIST_SEARCH + "/" + title + "/" + page);
  // .then((artistSearch) => {
  //   // console.log({ artistSearch });
  //   returnData(artistSearch.data);
  //   // loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default artistSearch;
