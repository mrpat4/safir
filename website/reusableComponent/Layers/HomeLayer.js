import React from 'react';
import Footer from '../../baseComponent/Footer/FooterCard';
import Header from '../../baseComponent/Header';

const Layer = (props) => {
  return (
    <div className=' px-0'>
      <Header />
      {props.children}
      <Footer />
    </div>
  );
};

export default Layer;
