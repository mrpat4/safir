import React, { useRef, useEffect, useState, Fragment } from "react";
import { post } from "../../../api";
import FormInputProduct from "./FormInputProduct";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";

const AddProduct = (props) => {
  const { editData } = props;
  console.log({ editData });

  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addProduct });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  // const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
  });
  const [imageAccepted, setImageAccepted] = useState();

  const [checkSubmitted, setCheckSubmitted] = useState(false);

  useEffect(() => {
    let arrayData = [];
    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) {
          if (stateArray[index].id === key)
            if (key === "category") arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
            else arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
        }
    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);
  // ============================================================================= modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    // handelgetApi();
  };
  // console.log(ModalInpts);

  const onShowModal = (event) => {
    if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: null });
    else if (event ? event.kindOf === "showGallery" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: event.name });
  };
  // ============================= remove
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================

  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    console.log({ imageAccepted, images });

    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };

  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "image", value: imageAccepted });
  };

  const renderModalInputs = (
    <div className={"width80"}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.kindOf === "showGallery" && <Gallery clickedParent={(value) => inputChangedHandler({ name: ModalInpts.name, value })} acceptedCardInfo={{ handelAcceptedImage: acceptedImage, acceptedCard: imageAccepted }} />}
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />}
        {ModalInpts.kindOf === "question" ? (
          ""
        ) : (
          <div className="btns-container">
            <button disabled={!imageAccepted} className="btns btns-primary" onClick={(e) => acceptedImageFinal(e)}>
              تایید{" "}
            </button>
            <button className="btns btns-warning" onClick={onHideModal}>
              بستن{" "}
            </button>
          </div>
        )}
      </ModalBox>
    </div>
  );
  // ========================= End modal =================
  // ============================= submited
  const _onSubmitted = async (e) => {
    setCheckSubmitted(!checkSubmitted);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    console.log({ formData });
    if (await post.product(formData)) setData({ ...states.addProduct });
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "thumbnail" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputProduct removeHandel={removeHandel} _onSubmited={_onSubmitted} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmitted} showModal={onShowModal} />;
  return (
    <div className="countainer-main">
      {renderModalInputs}
      <div style={{ marginTop: "20px" }} className="form-countainer">
        <div className="form-subtitle">افزودن جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmitted}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddProduct;
