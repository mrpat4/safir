import React from "react";

const TwoCheckBox = (props) => {
  const { accepted } = props;
  console.log({ props });

  return (
    <>
      <div className="margin-1rem centerAll">
        <label>{" بله : "}</label>
        <input onClick={() => accepted("true")} type="radio" name="group1" />
      </div>
      <div className="margin-1rem centerAll">
        <label>{" خیر : "}</label>
        <input onClick={() => accepted("false")} type="radio" name="group1" />
      </div>
    </>
  );
};

export default TwoCheckBox;
