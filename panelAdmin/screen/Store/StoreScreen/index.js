import React, { useState } from "react";
import panelAdmin from "../../..";
import StoreTableElement from "./StoreTableElement";
import AddStore from "../AddStore";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
const StoreScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData: ApiData } = props;
  console.log({ ApiData });
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });

  const card = panelAdmin.utils.consts.card;
  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    if (await panelAdmin.api.deletes.product(id)) {
      apiPageFetch(ApiData.page);
      onHideModal();
    }
  };
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal

  const tableOnclick = (index, kindOf) => {
    switch (kindOf) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: ApiData.docs[index]._id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: ApiData.docs[index] });
        break;
      default:
        break;
    }
  };
  return (
    <React.Fragment>
      {/* {renderModalInputs} */}
      <ModalStructure modalRequest={modalRequest} reqApiRemove={reqApiRemove} onHideModal={onHideModal} modalDetails={modalDetails}>
        {modalDetails.kindOf === "component" && <AddStore propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <StoreTableElement requestData={ApiData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={null} />
    </React.Fragment>
  );
};

export default StoreScreen;
