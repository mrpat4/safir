import axios from "../../../utils/axiosBase";
import panelAdmin from "../..";

const genres = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  const axiosData = page ? strings.GENRES + "/" + page : strings.GENRES;
  return axios.get(axiosData);
  // .then((genres) => {
  //   console.log({ genres });
  //   returnData(genres.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default genres;
