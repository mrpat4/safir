import React from "react";
import updateObject from "../../../../../utils/updateObject";

const Submitted = async (props) => {
  const { setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};
  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  const initialStatePhone = updateObject(states.addOwner.Form["phone"], { value: [] });
  const initialStateCoordinate = updateObject(states.addOwner.Form["coordinate"], { value: { lat: "", lng: "" } });
  const updatedForm = updateObject(states.addOwner.Form, { ["coordinate"]: initialStateCoordinate, ["phone"]: initialStatePhone });
  if (editData) {
    if (await put.owner({ id: editData._id, data: formData })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.owner(formData)) setData({ Form: updatedForm, formIsValid: false });

  setSubmitLoading(false);
};

export default Submitted;
