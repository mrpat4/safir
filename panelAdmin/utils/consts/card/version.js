import React from "react";

const version = (data) => {
  const cardFormat = [];
  console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let link = dataIndex.link ? dataIndex.link : "";
    let version = dataIndex.version ? dataIndex.version : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: false,
      body: [
        {
          right: [{ elementType: "text", value: link, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          left: [{ elementType: "text", value: version, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }

  return cardFormat;
};

export default version;
