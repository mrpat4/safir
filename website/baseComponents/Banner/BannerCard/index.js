import React from "react";
import Link from "next/link";

const BannerCard = ({ data }) => {
  const { image, title, desc } = data;
  return (
    <figure className="col-12">
      <Link href="#">
        <a>
          <img src={image} alt="photo" />
          <div className="title-wrapper">
            <h2>{title}</h2>
            <h3>{desc}</h3>
          </div>
        </a>
      </Link>
    </figure>
  );
};
export default BannerCard;
