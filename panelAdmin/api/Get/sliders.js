import axios from "../../../globalUtils/axiosBase";
import panelAdmin from "../..";

const sliders = async (page) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  const axiosData = page ? strings.SLIDER + "/" + page : strings.SLIDER;
  return axios
    .get(axiosData)
    .then((sliders) => {
      console.log({ sliders });
      return sliders.data;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default sliders;
