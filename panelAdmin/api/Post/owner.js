import panelAdmin from "../..";
import globalUtils from "../../../globalUtils";
const owner = async (param, setLoading) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  const axios = globalUtils.axiosBase;

  let URL = strings.OWNERS;
  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};
export default owner;
