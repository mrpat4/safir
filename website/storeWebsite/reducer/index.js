import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import homeReducer from "./homeReducer";
import themeColorReducer from "./themeColorReducer";
import filterReducer from "./filterReducer";

const rootReducer = {
  error: errorReducer,
  home: homeReducer,
  themeColor: themeColorReducer,
  filter: filterReducer,
};

export default rootReducer;
