import React, { useState, useMemo, useEffect } from "react";
import SideMenu from "../component/SideMenu";
import Header from "../component/Header";
import BackgrandCover from "../component/UI/BackgrandCover";
import { ToastContainer, toast } from "react-toastify";
import Scrollbar from "react-scrollbars-custom";
import reducer from "../../_context/reducer";
import { useRouter } from "next/router";
const OptionReducer = reducer.panelAdminReducer.optionReducerProvider;
const PanelScreen = (props) => {
  const { router, prefetch } = props;
  const [sidebarToggle, setSidebarToggle] = useState(false);

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };
  // const router = useRouter();
  // useEffect(() => {
  //   if (prefetch) router.prefetch(href);
  // });
  return (
    <OptionReducer>
      <div className={`panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`}>
        <div style={{ direction: "rtl", display: "flex" }}>
          <SideMenu windowLocation={router.asPath} />
          <div className="panelAdmin-container">
            <Header _handelSidebarToggle={_handelSidebarToggle} />
            {/* <Scrollbar style={{ width: "100%", height: " 100% " }}> */}
            <div className="panelAdmin-content">{props.children}</div>
            {/* </Scrollbar> */}
          </div>
        </div>
        <BackgrandCover fadeIn={sidebarToggle} onClick={_handelSidebarToggle} />
        <ToastContainer rtl />
      </div>
    </OptionReducer>
  );
};

export default PanelScreen;
