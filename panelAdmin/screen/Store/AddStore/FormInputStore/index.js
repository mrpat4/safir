import React, { useEffect, useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
// import FormMap from "../FormMap";
const FormInputStore = (props) => {
  const { stateArray, removeHandel, state, inputChangedHandler, showModal, checkSubmited } = props;
  const [uploadWeb, setUploadWeb] = useState(false);
  const [Category, setCategory] = useState(false);
  const [searchOwner, setSearchOwner] = useState(false);

  const accept = (event) => {
    setSearchOwner([]);
    inputChangedHandler(event);
  };

  useEffect(() => {
    setUploadWeb(true);
    getApi();
  }, []);

  let searchOwnerData = [];
  for (const index in searchOwner) searchOwnerData.push({ value: searchOwner[index]._id, title: searchOwner[index].title, description: searchOwner[index].subTitle, image: searchOwner[index].thumbnail });
  console.log({ searchOwnerData, searchOwner });

  const searchedOwner = async (e) => {
    if (e.currentTarget.value.length >= 2) {
      const resSearchOwner = await panelAdmin.api.get.ownersSearch(e.currentTarget.value);
      if (resSearchOwner) setSearchOwner(resSearchOwner);
    }
    // get.ownersSearch(e.currentTarget.value, setSearchOwner);
  };

  const getApi = async (page = "1") => {
    const resCategory = await panelAdmin.api.get.categories();
    setCategory(resCategory.data);
    //  get.category(setCategory, page, setloading);
  };

  //   =================== onChange Inputs

  let categoryData = [];
  for (const index in Category) categoryData.push({ value: Category[index]._id, title: Category[index].titleFa, description: Category[index].titleEn });
  console.log({ Category, categoryData });

  return (
    <form>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress, disabled, dropDownData;
        let value = formElement.config.value;
        disabled = false;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "slides") {
          value = formElement.config.value ? formElement.config.value : "";
          disabled = true;
          accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
        } else if (formElement.id === "coordinate") {
          changed = (e, child) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, child });
        } else if (formElement.id === "category") {
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = categoryData;
        } else if (formElement.id === "owner") {
          changed = searchedOwner;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = searchOwnerData;
        } else {
          changed = (e) =>
            inputChangedHandler({
              value: e.currentTarget.value,
              name: formElement.id,
              type: e.currentTarget.type,
              files: e.currentTarget.files,
            });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
            disabled={disabled}
            dropDownData={dropDownData}
          />
        );

        // if (formElement.id === "coordinate") {
        //   if (uploadWeb) form = <FormMap uploadWeb={uploadWeb} formElement={formElement} inputChangedHandler={inputChangedHandler} inputClasses={inputClasses} />;
        // }
        return form;
      })}
    </form>
  );
};

export default FormInputStore;
