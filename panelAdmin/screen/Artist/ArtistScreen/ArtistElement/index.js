import React from "react";
import Pageination from "../../../../component/UI/Pagination";
import InputVsIcon from "../../../../component/UI/InputVsIcon";
import panelAdmin from "../../../..";
// import IsNull from "../../../../components/UI/IsNull";
import TabelBasic from "../../../../component/UI/Tables";
const ArtistElement = (props) => {
  const { Artist, handelPage, tableOnclick, handelChange } = props;
  return (
    <div className="countainer-main  ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <TabelBasic
          subTitle={
            <div className="disColum ">
              <h4
                style={{
                  color: "black",
                  paddingBottom: "0.2em",
                  fontSize: "1.5em",
                }}
              >
                {" هنرمندان "}
              </h4>
              {Artist ? <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${Artist.total}  `}</span> : " "}
            </div>
          }
          imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
          tbody={panelAdmin.utils.consts.table.artist(Artist ? Artist.docs : "").tbody}
          thead={panelAdmin.utils.consts.table.artist(Artist ? Artist.docs : "").thead}
          onClick={tableOnclick}
          inputRef
          searchHead={InputVsIcon({
            name: "genreTitle",
            icon: "far fa-search",
            placeholder: "Name,...",
            onChange: handelChange,
            dir: "ltr",
          })}
        />

        {Artist && Artist.pages >= 2 && <Pageination limited={"3"} pages={Artist ? Artist.pages : ""} activePage={Artist ? Artist.page : ""} onClick={handelPage} />}
      </div>
    </div>
  );
};

export default ArtistElement;
