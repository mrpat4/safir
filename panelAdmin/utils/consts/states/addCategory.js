const owner = {
  Form: {
    titleFa: {
      label: "نام دسته فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته فارسی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    titleEn: {
      label: "نام دسته انگلیسی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته انگلیسی ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default owner;
