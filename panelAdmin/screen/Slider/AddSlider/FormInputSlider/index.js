import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
const FormInputSlider = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;
  const [searchStore, setSearchStore] = useState(false);
  const accept = (event) => {
    setSearchStore([]);
    inputChangedHandler(event);
  };
  let searchStoreData = [];
  for (const index in searchStore) searchStoreData.push({ value: searchStore[index]._id, title: searchStore[index].title, description: searchStore[index].subTitle, image: searchStore[index].slides[0] });
  console.log({ searchStoreData, searchStore });

  const searchedStore = async (e) => {
    if (e.currentTarget.value.length >= 2) {
      const resSearchStore = await panelAdmin.api.get.storeSearch(e.currentTarget.value);
      console.log({ resSearchStore });

      if (resSearchStore) setSearchStore(resSearchStore);
    }
    // get.StoresSearch(e.currentTarget.value, setSearchStore);
  };

  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress, disabled, dropDownData;
        let value = formElement.config.value;
        disabled = false;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "image") {
          // progress = state.progressPercentImage;
          value = formElement.config.value ? formElement.config.value : "";
          disabled = true;
          accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
        } else if (formElement.id === "parent") {
          changed = searchedStore;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = searchStoreData;
        } else {
          changed = (e) =>
            inputChangedHandler({
              value: e.currentTarget.value,
              name: formElement.id,
              type: e.currentTarget.type,
              files: e.currentTarget.files,
            });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
            disabled={disabled}
            dropDownData={dropDownData}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputSlider;
