import panelAdminReducer from "./panelAdminReducer";
import webSiteReducer from "./webSiteReducer";

const reducer = {
  panelAdminReducer,
  webSiteReducer,
};
export default reducer;
