import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const song = async (returnData, page, type, loading) => {
  return axios
    .get(Strings.ApiString.SONG + "/" + page)
    .then((song) => {
      console.log({ song });
      returnData(song.data);
      loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default song;
