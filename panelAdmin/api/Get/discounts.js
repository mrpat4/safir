import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const discounts = async (returnData, page, loading) => {
  return axios
    .get(Strings.ApiString.DISCOUNT + "/" + page)
    .then((discounts) => {
      // console.log({ discounts });
      returnData(discounts.data);
      loading(false);
    })
    .catch((error) => {
      // console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default discounts;
