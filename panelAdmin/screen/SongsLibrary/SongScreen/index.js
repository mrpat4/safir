import React, { useEffect, useState, Fragment, useRef } from "react";
import { get } from "../../../api";
import ModalBox from "../../../util/modals/ModalBox";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import SongElement from "./SongElement";
import AddSong from "../AddSong";
import ModalSrc from "../../../util/modals/ModalSrc";
import AudioPlayer from "../../../components/AudioPlayer";
const SongScreen = () => {
  const [Song, setSong] = useState();
  const [SongSearch, setSongSearch] = useState();
  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });

  const [Loading, setLoading] = useState(true);
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  useEffect(() => {
    handelgetApi();
  }, [state.valueEn]);
  const handelgetApi = (page = "1", type = state.valueEn) => {
    setEditData(false);
    get.song(setSong, page, type, setLoading);
  };
  console.log({ Song });

  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    // if (value) if (await deletes.category(state.remove.id)) handelgetApi();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal({ kindOf: "remove" });
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    handelgetApi();
    setEditData();
  };
  console.log(ModalInpts);

  const onShowlModal = (event) => {
    if (event ? event.kindOf === "src" : false) setModalInpts({ ...ModalInpts, show: true, data: { src: event.src, type: event.type, name: event.name }, kindOf: event.kindOf });
    else if (event ? event.kindOf === "addData" || event.kindOf === "editData" : false) setModalInpts({ ...ModalInpts, show: true, data: { src: false, kindOf: false, name: false }, kindOf: event.kindOf });
  };
  const renderModalInputs = (
    <div className={"bgUnset"}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.kindOf === "addData" && <AddSong getApi={handelgetApi} InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />}
        {ModalInpts.kindOf === "src" && <AudioPlayer src={ModalInpts.data.src} />}
      </ModalBox>
    </div>
  );
  const modalWork = ({ work, index }) => {
    if (work === "add") {
      onShowlModal({ kindOf: "addData" });
    } else if (work === "remove") {
      // removeHandel(Song.docs[index]._id);
    } else if (work === "play") {
      console.log(Song.docs[index]);

      let acceptSong = Song.docs[index];

      onShowlModal({ kindOf: "src", src: acceptSong.href, name: "VOICE" });
    }
  };
  const _handelPage = (value) => {
    if (value) handelgetApi(value);
  };
  const handelchange = (event) => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;
    setState({ ...state, [name]: value });
    get.songSearch(value, "1", setSongSearch);
  };
  console.log({ SongSearch });

  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <Fragment>
      {renderModalInputs}
      <SongElement state={state} setState={setState} Song={SongSearch ? SongSearch : Song} handelPage={_handelPage} handelchange={handelchange} showModal={modalWork} />
    </Fragment>
  );
};

export default SongScreen;
