import en from "./en";
import fa from "./fa";
const Lang = "rtl";
let strings;
if (Lang === "rtl") strings = fa;
else strings = en;

export default strings;
