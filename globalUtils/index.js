import Loading from "./Loading";
import axiosBase from "./axiosBase";
const globalUtils = {
  Loading,
  axiosBase,
};

export default globalUtils;
