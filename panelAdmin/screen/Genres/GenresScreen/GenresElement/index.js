import React from "react";
import table from "../../../../util/consts/table";
import Pageination from "../../../../components/UI/Pagination";
import InputVsIcon from "../../../../components/UI/InputVsIcon";
import TabelBasic from "../../../../components/Tables";
import IsNull from "../../../../components/UI/IsNull";

const GenresElement = (props) => {
  const { Genres, handelPage, tableOnclick, handelchange } = props;
  return (
    <div className="countainer-main  ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <TabelBasic
          subTitle={
            <div className="disColum ">
              <h4 style={{ color: "black", paddingBottom: "0.2em", fontSize: "1.5em" }}>{" ژانر ها"}</h4>
              <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${Genres.total}  `}</span>{" "}
            </div>
          }
          imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
          tbody={table.genres(Genres.docs).tbody}
          thead={table.genres(Genres.docs).thead}
          onClick={tableOnclick}
          inputRef
          searchHead={InputVsIcon({ name: "genreTitle", icon: "far fa-search", placeholder: "pop,rock,...", onChange: handelchange, dir: "ltr" })}
        />

        {Genres && Genres.pages >= 2 && <Pageination limited={"3"} pages={Genres ? Genres.pages : ""} activePage={Genres ? Genres.page : ""} onClick={handelPage} />}
      </div>
    </div>
  );
};

export default GenresElement;
