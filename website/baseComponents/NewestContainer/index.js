import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import cartActions from "../../../store/actions/redux/cartActions";
import AwesomeScroll from "../../../components/AwesomeScroll";
import { Container, Row } from "react-bootstrap";

// import Section from "../../../reusableComponent/Section";

const NewestContainer = ({ data }) => {
  const svgOrange = "svg-orange";

  const productsList = _.chunk(data, 2);
  // TODO conncet to databas with saga to get popular data
  return (
    <div className="element-container-col" style={{ backgroundColor: "rgba(248, 147, 31,0.5)" }}>
      <div className={svgOrange}></div>
      <Container>
        <AwesomeScroll scrollBar>
          <Row as="ul" onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper mx-0">
            {productsList.map((info, index) => {
              return <DoubleProductCard key={"DoubleProductCard-" + index} info={info} />;
            })}
          </Row>
        </AwesomeScroll>
      </Container>
    </div>
  );
};

export default NewestContainer;
