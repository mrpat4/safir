const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";

const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";

const GALLERY = "گالری";
const GALLERIES = "گالری ها";
const ADD_GALLERY = "افزودن گالری";
const SEE_GALLERIES = "مشاهده گالری ها";

const CATEGORY = "دسته بندی";
const CATEGORIES = "دسته بندی ها";
const ADD_CATEGORY = "افزودن دسته بندی";
const SEE_CATEGORIES = "مشاهده دسته بندی ها";

const PRODUCT = "محصول ";
const ADD_PRODUCT = "افزودن محصول";
const SEE_PRODUCTS = "مشاهده محصولات ";

const OWNER = "فروشنده ";
const ADD_OWNER = "افزودن فروشنده";
const SEE_OWNERS = "مشاهده فروشندگان ";

const STORE = "فروشگاه ";
const ADD_STORE = "افزودن فروشگاه";
const SEE_STORES = "مشاهده فروشگاه ها ";

const SLIDER = "اسلایدر ";
const ADD_SLIDER = "افزودن اسلایدر";
const SEE_SLIDERS = "مشاهده اسلایدر ها ";

const BANNER = "بنر ";
const ADD_BANNER = "افزودن بنر";
const SEE_BANNERS = "مشاهده بنر ها ";

const NOTIFICATION = "اعلان ";
const ADD_NOTIFICATION = "افزودن اعلان";
const SEE_NOTIFICATIONS = "مشاهده اعلان ها ";

const VERSION = "ورژن ";
const ADD_VERSION = "افزودن ورژن";
const SEE_VERSIONS = "مشاهده ورژن ها ";

const sideMenu = {
  DASHBOARD,

  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,

  SETTING_WEB,

  CATEGORIES,

  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,

  GALLERY,
  GALLERIES,
  ADD_GALLERY,
  SEE_GALLERIES,

  CATEGORY,
  ADD_CATEGORY,
  SEE_CATEGORIES,

  PRODUCT,
  ADD_PRODUCT,
  SEE_PRODUCTS,

  OWNER,
  ADD_OWNER,
  SEE_OWNERS,

  STORE,
  ADD_STORE,
  SEE_STORES,

  STORE,
  ADD_STORE,
  SEE_STORES,

  SLIDER,
  ADD_SLIDER,
  SEE_SLIDERS,

  BANNER,
  ADD_BANNER,
  SEE_BANNERS,

  NOTIFICATION,
  ADD_NOTIFICATION,
  SEE_NOTIFICATIONS,

  VERSION,
  ADD_VERSION,
  SEE_VERSIONS,
};
export default sideMenu;
