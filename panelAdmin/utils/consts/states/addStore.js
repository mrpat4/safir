const addStore = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    installments: {
      label: "اقساط :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اقساط ",
      },
      value: "",
      validation: {
        required: true,
        isNumeric: true,
      },
      valid: false,
      touched: false,
    },

    commission: {
      label: "کمیسیون :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کمیسیون ",
      },
      value: "",
      validation: {
        required: true,
        isNumeric: true,
      },
      valid: false,
      touched: false,
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    areaCode: {
      label: "کد فروشگاه :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کد فروشگاه ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    phoneNumber: {
      label: "تلفن همراه :",

      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تلفن همراه",
      },
      value: "",
      validation: {
        required: true,
        minLength: 11,
        maxLength: 11,
        isNumeric: true,
        isMobile: true,
      },
      valid: false,
      touched: false,
    },
    phone: {
      label: "تلفن ثابت :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "تلفن ثابت",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],
      valid: false,
      touched: false,
    },
    district: {
      label: "ناحیه :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ناحیه",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    aboutStore: {
      label: "درباره فروشگاه :",

      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره فروشگاه",
      },
      value: "",
      validation: {
        // required: true,
      },
      valid: true,
      touched: true,
    },
    // rating: {
    //   label: "امتیاز :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "امتیاز"
    //   },
    //   value: "",
    //   validation: {
    //     minLength: 1,
    //     maxLength: 1,
    //     isNumeric: true,
    //     required: true
    //   },
    //   valid: false,
    //   touched: false
    // },
    slides: {
      label: "اسلاید :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    coordinate: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: { lat: "", lng: "" },

      elementConfig: {
        type: "number",
        placeholder: "مختصات",
      },
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addStore;
