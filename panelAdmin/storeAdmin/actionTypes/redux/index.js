const atRedux = {
  //======================================================== Redux
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  //======================================================== LOADING
  CHANGE_LOADING_ALL: "CHANGE_LOADING_ALL_REDUX",
  CHANGE_LOADING_SEARCH: "CHANGE_LOADING_SEARCH_REDUX",
  // ======================================================== NAVBAR
  SET_PAGE_NAME: "SET_PAGE_NAME",
  // ======================================================== UPLOAD
  SET_UPLOAD_IMAGE: "SET_UPLOAD_IMAGE_REDUX",
  // ======================================================== GALLERY
  SET_GALLERY_DATA: "SET_GALLERY_DATA_REDUX",
  CHANGE_ADD_GALLERY_DATA: "CHANGE_ADD_GALLERY_DATA_REDUX",
  // ======================================================== CATEGORY
  SET_CATEGORY_DATA: "SET_CATEGORY_DATA_REDUX",
  SET_SEARCH_CATEGORY_DATA: "SET_SEARCH_CATEGORY_DATA_REDUX",
  // ======================================================== PRODUCT
  SET_PRODUCT_DATA: "SET_PRODUCT_DATA_REDUX",
  SET_SEARCH_PRODUCT_DATA: "SET_SEARCH_PRODUCT_DATA_REDUX",
};

export default atRedux;
