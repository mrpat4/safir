import React, { useState } from "react";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
import Table from "../../../component/UI/Tables/Table";
import ProductTableElement from "./ProductTableElement";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import AddProduct from "../AddProduct";
const ProductScreen = (props) => {
  const { apiPageFetch, onDataSearch, ApiData } = props;
  console.log({ ApiData });

  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [state, setState] = useState({
    remove: { index: "", name: "" },
    ArtistTitle: "",
  });

  const card = panelAdmin.utils.consts.card;

  // ======================================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false, removeId: null });
    // handelgetApi();
  };
  // console.log(ModalInpts);

  const onShowModal = (event) => {
    if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: null, removeId: event.removeId });
    else if (event ? event.kindOf === "edit" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: event.name, editData: event.editData });
  };
  // ============================= remove
  const __returnPrevStep = async (value) => {
    console.log({ value });
    if (value) {
      if (await panelAdmin.api.deletes.product(ModalInpts.removeId)) apiPageFetch(ApiData.page);
      onHideModal();
    } else onHideModal();
  };
  // =========================== End remove  ====================

  const renderModalInputs = (
    <div className={ModalInpts.kindOf === "edit" ? "width80" : ""}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevStep} />}
        {ModalInpts.kindOf === "edit" && <AddProduct editData={ModalInpts.editData} modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevStep} />}
      </ModalBox>
    </div>
  );
  // ========================================END modal

  const tableOnclick = (index, kindOf) => {
    console.log({ index, kindOf });
    console.log(ApiData.docs[index]._id);

    switch (kindOf) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: ApiData.docs[index]._id });
        break;
      case "edit":
        onShowModal({ kindOf: "edit", editData: ApiData.docs[index] });
        break;
      default:
        break;
    }
  };
  return (
    <React.Fragment>
      {renderModalInputs}

      <ProductTableElement ApiData={ApiData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={null} />
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
      {/* {data.loading.loadingAll && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )} */}
    </React.Fragment>
  );
};

export default ProductScreen;
