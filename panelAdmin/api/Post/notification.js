import axios from "../../../globalUtils/axiosBase";
import panelAdmin from "../..";

const notification = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  let URL = strings.NOTIFICATION;
  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });
      // setLoading(false);

      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      // setLoading(false);

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data)
        switch (error.response.data.Error) {
          case 1016:
            toastify("وزن تکراری می باشد", "error");
            break;
          case 1017:
            toastify("عنوان انگلیسی تکراری می باشد", "error");
            break;
          case 1018:
            toastify("عنوان فارسی تکراری می باشد", "error");
            break;
          default:
            toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
            break;
        }
      return false;
    });
};
export default notification;
