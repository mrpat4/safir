import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const scenarios = async (returnData, page, loading) => {
  return axios
    .get(Strings.ApiString.SCENARIO + "/" + page)
    .then((scenario) => {
      console.log({ scenario });
      returnData(scenario.data);
      loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default scenarios;
