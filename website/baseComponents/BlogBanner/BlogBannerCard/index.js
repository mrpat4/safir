import React from "react";
import Link from "next/link";

const BlogBannerCard = ({ data }) => {
  const { image, title, desc } = data;
  return (
    <figure className="p-0">
      <img src={image} alt="photo" />
      <div className="title-wrapper">
        <h2>{title}</h2>
        <p>{desc}</p>
        <Link href="#">
          <a>
            {" "}
            <span> مطالعه بیشتر</span>
          </a>
        </Link>
      </div>
    </figure>
  );
};
export default BlogBannerCard;
