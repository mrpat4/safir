import React from "react";
import formatMoney from "../../formatMoney";

const ShowTransactions = (data) => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
    let totalPrice = data[index].totalPrice ? formatMoney(data[index].totalPrice) : "0";
    let showDiscount = { option: { eye: true, name: "discounts" } };
    let paymentStatus = data[index].paymentStatus ? <span style={{ color: "green" }}>{"پرداخت شده"}</span> : <span style={{ color: "red" }}> {"پرداخت نشده"}</span>;
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: { background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : "" },
    });
  }
  return [thead, tbody];
};

export default ShowTransactions;
