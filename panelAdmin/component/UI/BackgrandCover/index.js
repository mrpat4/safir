import React from "react";

const BackgrandCover = (props) => {
  return (
    <div
      id="coverContainer"
      onClick={props.onClick}
      className={props.fadeIn ? " fadeIn" : " fadeOut"}
    ></div>
  );
};

export default BackgrandCover;
