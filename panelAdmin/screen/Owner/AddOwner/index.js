import React, { useRef, useEffect, useState, Fragment } from "react";
import { post, put } from "../../../api";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";
import FormInputOwner from "./FormInputOwner";
import LoadingDot1 from "../../../component/UI/Loadings/LoadingDot1";
import ModalOption from "./DependentComponent/ModalOption";
import Submitted from "./DependentComponent/Submitted";
import EditData from "./DependentComponent/EditData";
const AddOwner = (props) => {
  const { editData, setEdit, propsHideModal } = props;
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addOwner });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [imageAccepted, setImageAccepted] = useState();
  const [checkSubmitted, setCheckSubmitted] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  // ========================================================= change edit data structure for state
  useEffect(() => {
    EditData({ editData, stateArray, inputChangedHandler, setStaticTitle });
  }, [editData]);
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
    onHideModal();
  };
  // ========================================================= END modal
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // ========================================================= accepted image for add data
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "thumbnail", value: imageAccepted });
  };
  // ========================================================= End accepted image for add data

  // ============================= submited
  const _onSubmitted = async (e) => {
    e.preventDefault();
    Submitted({ setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted });
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "flag" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputOwner staticTitle={staticTitle} removeHandel={removeHandel} showModal={onShowModal} _onSubmited={_onSubmitted} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmitted} />;
  return (
    <div className="countainer-main  ">
      <ModalOption {...{ inputChangedHandler, modalRequest, onHideModal, modalDetails, imageAccepted, acceptedImageFinal, acceptedImage, Gallery }} />
      <div className="form-countainer">
        {/* <div className="form-subtitle">{"افزودن دسته جدید"}</div> */}
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid || submitLoading} onClick={_onSubmitted}>
              {submitLoading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : "افزودن"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddOwner;
