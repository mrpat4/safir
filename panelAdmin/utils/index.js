import consts from "./consts";
import toastify from "./toastify";
import CelanderConvert from "./CelanderConvert";
import onChanges from "./onChanges";
import handleKey from "./handleKey";
import formatMoney from "./formatMoney";
import json from "./json";
import updateObject from "./updateObject";

const utils = { consts, toastify, CelanderConvert, onChanges, handleKey, formatMoney, json, updateObject };
export default utils;
