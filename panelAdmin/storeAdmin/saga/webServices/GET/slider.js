import axios from "axios";
import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";
export function* sliderData({ data }) {
  try {
    const res = yield panelAdmin.api.get.slider(...data);
    console.log({ res });

    yield put(actions.reduxActions.setSliderData(res.data)); //********** Level 5 **********//
  } catch (err) {
    console.log({ err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
