import React from "react";

const owner = (data) => {
  console.log({ ownerdata: data });

  const thead = ["#", "عکس", "نام ", "محدوده  ", " شماره همراه", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let district = data[index].district ? data[index].district : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let balance = data[index].balance ? data[index].balance : "0";
    let isActive = data[index].isActive ? active : deActive;
    // , { option: { star: true, value: rating } }
    tbody.push({
      data: [thumbnail, title, district, phoneNumber, isActive, { option: { edit: true } }],
      style: {},
    });
  }

  return { thead, tbody };
};

export default owner;
