import React from "react";
import Link from "next/link";
import { Col } from "react-bootstrap";

const DoubleBannerCard = ({ props }) => {
  const { image, title } = props;
  return (
    <Col as="figure" xl={6} lg={6} md={6} sm={12} xs={12} className="">
      <div>
        <img src={image} alt="photo" />
        <figcaption>
          <h3>{title}</h3>
          <Link href="#">
            <a>خرید</a>
          </Link>
        </figcaption>
      </div>
    </Col>
  );
};
export default DoubleBannerCard;
