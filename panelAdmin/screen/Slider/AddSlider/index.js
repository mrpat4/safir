import React, { useRef, useEffect, useState, Fragment } from "react";
import { post } from "../../../api";
import FormInputSlider from "./FormInputSlider";
import panelAdmin from "../../..";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import Gallery from "../../../../pages/panelAdmin/gallery";
import LoadingDot1 from "../../../component/UI/Loadings/LoadingDot1";
const AddSlider = () => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addSlider });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [editData, setEditData] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    kindOf: false,
    data: { src: false, type: false, name: false },
    name: null,
  });
  const [imageAccepted, setImageAccepted] = useState();

  const [checkSubmitted, setCheckSubmitted] = useState(false);
  // ======================================== modal

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, kindOf: false });
    // handelgetApi();
    setEditData();
  };
  // console.log(ModalInpts);

  const onShowModal = (event) => {
    if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: null });
    else if (event ? event.kindOf === "showGallery" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf, name: event.name });
  };
  // ============================= remove
  const __returnPrevstep = (value) => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    // console.log({ name: "image", value: imageAccepted });

    inputChangedHandler({ name: "image", value: imageAccepted });
  };
  const renderModalInputs = (
    <div className={"width80"}>
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {
          ModalInpts.kindOf === "showGallery" && <Gallery clickedParent={(value) => inputChangedHandler({ name: ModalInpts.name, value })} acceptedCardInfo={{ handelAcceptedImage: acceptedImage, acceptedCard: imageAccepted }} />
          // <ShowListData data={""}/>
        }
        {/* {ModalInpts.kindOf === "showGallery" && <ShowListData getApi={handelgetApi} InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />} */}
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />}
        {ModalInpts.kindOf === "question" ? (
          ""
        ) : (
          <div className="btns-container">
            <button disabled={!imageAccepted} className="btns btns-primary" onClick={(e) => acceptedImageFinal(e)}>
              تایید{" "}
            </button>
            <button className="btns btns-warning" onClick={onHideModal}>
              بستن{" "}
            </button>
          </div>
        )}
      </ModalBox>
    </div>
  );

  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    setSubmitLoading(true);
    setCheckSubmitted(!checkSubmitted);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (await post.slider(formData)) setData({ ...states.addSlider });

    setSubmitLoading(false);
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "flag" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputSlider showModal={onShowModal} _onSubmited={_onSubmited} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmitted} />;
  return (
    <div className="countainer-main  ">
      {renderModalInputs}
      <div className="form-countainer">
        <div className="form-subtitle">{"افزودن دسته جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid || submitLoading} onClick={_onSubmited}>
              {submitLoading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : "افزودن"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddSlider;
