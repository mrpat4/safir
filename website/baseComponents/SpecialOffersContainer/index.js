import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import cartActions from "../../../store/actions/redux/cartActions";
import AwesomeScroll from "../../../components/AwesomeScroll";
import { Container, Row } from "react-bootstrap";

const SpecialOffersContainer = ({ data }) => {
  const svgBlue = "svg-blue";

  // const onSelectedProduct = (data, count) => {
  //   const foundedPop = _.find(store, { id: data.id });
  //   foundedPop ? dispatch(cartActions.changeCount({ id: data.id, count })) : count != -1 && dispatch(cartActions.addToCart(data));
  // };

  const productsList = _.chunk(data, 2);

  //TODO conncet to databas with saga to get popular data
  return (
    <div className="element-container-col" style={{ backgroundColor: "#fffbcc" }}>
      <div className={svgBlue}></div>
      <Container>
        <AwesomeScroll scrollBar>
          <Row as="ul" onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper row mx-0">
            {productsList.map((data, index) => {
              return <DoubleProductCard key={"DoubleProductCard-" + index} info={data} />;
            })}
          </Row>
        </AwesomeScroll>
      </Container>
    </div>
  );
};
export default SpecialOffersContainer;
