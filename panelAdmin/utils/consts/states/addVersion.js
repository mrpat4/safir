const version = {
  Form: {
    version: {
      label: "ورژن :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ورژن",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    link: {
      label: "لینک  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "لینک ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    isRequired: {
      label: "آیا ضروری میباشد  :",
      elementType: "twoCheckBox",
      elementConfig: {
        type: "checkBox",
        placeholder: "آیا ضروری میباشد ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default version;
