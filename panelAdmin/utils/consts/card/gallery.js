const instrument = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let title = dataIndex.title ? dataIndex.title : "";
    let href = dataIndex.href ? dataIndex.href : "";
    let images = href;
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard === href ? "activeImage" : "") : "",
      image: { value: images },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default instrument;
