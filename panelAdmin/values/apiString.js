const ADMIN = "/admin";
const CATEGORY = ADMIN + "/category";
const PRODUCT = ADMIN + "/product";
const IMAGE = ADMIN + "/images";
const UPLOAD = ADMIN + "/upload";
const OWNERS = ADMIN + "/owner";
const STORE = ADMIN + "/store";
const SLIDER = ADMIN + "/slider";
const BANNER = ADMIN + "/banner";
const NOTIFICATION = ADMIN + "/notification";
const VERSION = ADMIN + "/version";

const apiString = {
  CATEGORY,
  PRODUCT,
  IMAGE,
  UPLOAD,
  OWNERS,
  STORE,
  SLIDER,
  BANNER,
  NOTIFICATION,
  VERSION,
};
export default apiString;
