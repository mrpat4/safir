import React, { useState, useEffect } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import { get } from "../../../../api";
const FormInputProduct = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;
  const [Category, setCategory] = useState();

  const accept = (event) => inputChangedHandler(event);

  useEffect(() => {
    getCategoryApi();
  }, []);
  const getCategoryApi = async () => {
    const catData = await get.categories({ page: "1" });
    setCategory(catData.data.docs);
  };

  // =========================================================================== SEARCH STRUCTURE FOR DROP DOWN

  // =========================== FOR DROP DOWN CATEGORY ===========================
  let CategoryData = [];
  if (Category)
    for (const index in Category)
      CategoryData.push({
        value: Category[index]._id,
        title: Category[index].name,
        description: Category[index].titleEn,
      });
  // =========================================================================== FORM
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement, index) => {
        // ================================= VARIABLE VALUES
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, dropDownData, disabled;
        disabled = false;
        let value = formElement.config.value;
        const inputClasses = ["InputElement"];
        // ================================= CHANGE ELEMENT AMOUNTS
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        if (formElement.id === "image") {
          value = formElement.config.value ? formElement.config.value : "";
          disabled = true;
          accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
        } else if (formElement.id === "category") {
          dropDownData = CategoryData;
          accepted = (value) => accept({ value, name: formElement.id });
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }
        // ================================= INPUTS
        let Form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
            disabled={disabled}
          />
        );

        return Form;
      })}
    </form>
  );
};

export default FormInputProduct;
