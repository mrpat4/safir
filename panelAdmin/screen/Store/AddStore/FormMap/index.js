import React, { useState, useEffect } from "react";
import SimpleExample from "../../../../component/SimpleExample";
const FormMap = (props) => {
  const { formElement, inputChangedHandler, inputClasses, openMapModal, uploadWeb } = props;
  const [map, setmap] = useState(false);
  const [display, setDisplay] = useState(true);
  const showMap = () => {
    setmap(!map);
  };
  useEffect(() => {
    if (map) setDisplay(false);
  }, [map]);
  const endAnimation = () => {
    // (!map ? setDisplay(true) : setDisplay(false))
  };

  let form;
  form = (
    <div className={"Input"}>
      <div className="title-wrapper">
        <label className={"Label"}>{"مختصات"}</label> {/* <div onClick={showMap} className={"acceptedInmap"}>
          انتخاب از روی نقشه<i className=" icon-location"></i>
        </div> */}
      </div>
      <div className="coordinate-wrapper">
        <div>
          <span>{"طول (lng): "}</span>
          <input disabled className={inputClasses.join(" ")} value={formElement.config.value.lng} name={"lng"} onChange={(e) => inputChangedHandler({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"} />
        </div>
        <div>
          <span>{"عرض (lat) : "}</span>
          <input disabled className={inputClasses.join(" ")} value={formElement.config.value.lat} name={"lat"} onChange={(e) => inputChangedHandler({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"} />
        </div>
      </div>
      <div
      // style={{ display: map ? "" : "none" }}>
      >
        {uploadWeb && <SimpleExample newPin center={formElement.config.value.length > 0 ? formElement.config.value : { lat: "37.3003561", lng: "49.6029556" }} onChange={(value) => inputChangedHandler({ name: formElement.id, value: value })} />}
      </div>
    </div>
  );
  return form;
};

export default FormMap;
