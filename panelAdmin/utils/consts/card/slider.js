const slider = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let type = dataIndex.type ? dataIndex.type : "";
    let parent = dataIndex.parent ? dataIndex.parent : false;
    let title = parent.title ? parent.title : "";
    let image = dataIndex.image ? dataIndex.image : "";
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard === image ? "activeImage" : "") : "",
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          left: [{ elementType: "text", value: type, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default slider;
