import React from "react";

const products = (data) => {
  console.log({ data });

  const thead = ["#", "عکس", "عنوان", "قیمت اصلی ", " قیمت جدید", "وزن", "تخفیف", "بازدید", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let image = data[index].image ? data[index].image : NotEntered;
    let name = data[index].name ? data[index].name : NotEntered;
    let realPrice = data[index].realPrice ? data[index].realPrice : NotEntered;
    let newPrice = data[index].newPrice ? data[index].newPrice : NotEntered;
    let weight = data[index].weight ? data[index].weight : "0";

    let discount = data[index].discount ? data[index].discount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [image, name, realPrice, newPrice, weight, viewCount, discount, { option: { edit: true, remove: true } }],
      style: {},
    });
  }
  return { thead, tbody };
};

export default products;
