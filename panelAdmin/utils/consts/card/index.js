// import instrument from "./instrument";
import gallery from "./gallery";
// import country from "./country";
// import mood from "./mood";
// import hashtag from "./hashtag";
// import genre from "./genre";
import product from "./product";
import category from "./category";
import slider from "./slider";
import banner from "./banner";
import version from "./version";

const card = {
  category,
  // instrument,
  gallery,
  // country,
  // mood,
  // hashtag,
  product,
  slider,
  banner,
  version,
  // genre
};
export default card;
