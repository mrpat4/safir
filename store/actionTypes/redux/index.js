const atRedux = {
  //======================================================== Redux

  SET_HOME_DATA: "SET_HOME_DATA_REDUX",
  ADD_FAILURE: "ADD_FAILURE_REDUX",
  REMOVE_FAILURE: "REMOVE_FAILURE_REDUX",
  ADD_TO_CART: "ADD_TO_CART_REDUX",
  CHANGE_ITEM_CART_COUNT: "CHANGE_ITEM_CART_COUNT_REDUX",
};

export default atRedux;
