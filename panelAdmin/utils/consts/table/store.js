import React from "react";

const store = (data) => {
  console.log({ storedata: data });

  const thead = ["#", "عکس", "نام ", "کمیسیون  ", "اقساط", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let commission = data[index].commission ? data[index].commission : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let installments = data[index].installments ? data[index].installments : "0";
    let isActive = data[index].isActive ? active : deActive;
    // , { option: { star: true, value: rating } }
    tbody.push({
      data: [thumbnail, title, commission, installments, isActive, { option: { edit: true } }],
      style: {},
    });
  }

  return { thead, tbody };
};

export default store;
