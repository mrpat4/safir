import genres from "./genres";
import showScenario from "./ShowScenario";
import ShowScenarioDataInModal from "./ShowScenarioDataInModal";
import ShowScenarioDataInModalTwo from "./ShowScenarioDataInModalTwo";
import owner from "./owner";
import ShowDiscount from "./ShowDiscount";
import ShowClub from "./ShowClub";
import showTransaction from "./ShowTransactions";
import transactionDiscount from "./transactionDiscount";
import members from "./members";
import memberTransaction from "./memberTransaction";
import memberDiscount from "./memberDiscount";
import country from "./country";
import instrument from "./instrument";
import song from "./song";
import products from "./products";
import store from "./store";

const table = {
  instrument,
  country,
  memberDiscount,
  memberTransaction,
  members,
  transactionDiscount,
  showTransaction,
  ShowClub,
  ShowDiscount,
  owner,
  showScenario,
  genres,
  ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo,
  song,
  products,
  store,
};
export default table;
