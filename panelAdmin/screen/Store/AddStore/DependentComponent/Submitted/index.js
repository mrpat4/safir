import React from "react";
import updateObject from "../../../../../utils/updateObject";

const Submitted = async (props) => {
  const { setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);

  const formData = {};
  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  const initialStatePhone = updateObject(states.addStore.Form["phone"], { value: [] });
  const initialStateCoordinate = updateObject(states.addStore.Form["coordinate"], { value: { lat: "", lng: "" } });
  const initialStateSlides = updateObject(states.addStore.Form["slides"], { value: [] });
  const updatedForm = updateObject(states.addStore.Form, { ["coordinate"]: initialStateCoordinate, ["phone"]: initialStatePhone, ["slides"]: initialStateSlides });
  if (editData) {
    if (await put.store({ id: editData._id, data: formData })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.store(formData)) setData({ Form: updatedForm, formIsValid: false });
  setSubmitLoading(false);
};

export default Submitted;
