import axios from "../../../globalUtils/axiosBase";
import panelAdmin from "../..";

const gallery = async ({ page }) => {
  console.log({ page });

  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  return axios.get(strings.IMAGE + "/" + page);
};

export default gallery;
