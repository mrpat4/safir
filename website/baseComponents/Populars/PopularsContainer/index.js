import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import SingleProductCard from "../../../reusableComponent/cards/SingleProductCard";
import cartActions from "../../../../store/actions/redux/cartActions";
import AwesomeScroll from "../../../../components/AwesomeScroll";
import { Container, Row } from "react-bootstrap";

// import cartActions from "../../../store/actions/redux/cartActions"

const PopularsContainer = ({ data }) => {
  // const onSelectedProduct = (id, count) => {
  //   const foundedPop = _.find(store, { id: id });
  //   if (foundedPop) {
  //     return dispatch(cartActions.changeCount({ id, count }));
  //   } else {
  //     if (count != -1) return dispatch(cartActions.addToCart({ id }));
  //   }
  // };

  //TODO conncet to databas with saga to get popular data
  return (
    <div className="element-container-col">
      <Container>
        {/* <AwesomeScroll data={populars} Row={SingleProductCard} onSelectedProduct={onSelectedProduct} /> */}
        <AwesomeScroll scrollBar>
          <Row as="ul" onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper  mx-0">
            {data.map((info, index) => {
              return <SingleProductCard key={"SingleProductCard-" + index} info={info} />;
            })}
          </Row>
        </AwesomeScroll>
      </Container>
    </div>
  );
};
export default PopularsContainer;
