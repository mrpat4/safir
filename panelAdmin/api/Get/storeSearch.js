import globalUtils from "../../../globalUtils";
import panelAdmin from "../..";

const storeSearch = async (param) => {
  console.log({ param });

  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.STORE + "/s/" + param;
  console.log({ strings });

  return axios
    .get(strings)
    .then((storeSearch) => {
      console.log({ storeSearch });

      return storeSearch.data;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default storeSearch;
