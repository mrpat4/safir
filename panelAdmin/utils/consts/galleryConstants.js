import React from "react";
import panelAdmin from "../..";

const galleryConstants = () => {
  const ConstantsEn = panelAdmin.values.en;
  const string = panelAdmin.values.strings;
  return [
    { value: ConstantsEn.ALBUM_CONSTANTS, title: string.ALBUM_CONSTANTS },
    {
      value: ConstantsEn.PLAY_LIST_CONSTANTS,
      title: string.PLAY_LIST_CONSTANTS,
    },
    { value: ConstantsEn.FLAG_CONSTANTS, title: string.FLAG_CONSTANTS },
    { value: ConstantsEn.SONG_CONSTANTS, title: string.SONG_CONSTANTS },
    { value: ConstantsEn.ARTIST_CONSTANTS, title: string.ARTIST_CONSTANTS },
    {
      value: ConstantsEn.INSTRUMENT_CONSTANTS,
      title: string.INSTRUMENT_CONSTANTS,
    },
    {
      value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
      title: string.MUSIC_VIDEO_CONSTANTS,
    },
    { value: ConstantsEn.MOOD_CONSTANTS, title: string.MOOD_CONSTANTS },
  ];
};
export default galleryConstants;
