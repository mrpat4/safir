import React, { useState} from "react";
import { useSelector } from "react-redux";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
const ArtistsScreen = (props) => {
  const { onDataChange, onDataSearch ,data} = props;
  const [state, setState] = useState({
    remove: { index: "", name: "" },
    ArtistTitle: "",
  });
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const store = useSelector((state) => {
    return state.artist;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const ArtistData = store.artistData;
  const searchArtistData = store.searchArtistData;
  const searchArtist = searchArtistData ? (searchArtistData.docs.length ? true : false) : false;
  const showDataElement = ArtistData && ArtistData.docs.length && (
    <ShowCardInformation data={card.artist(ArtistData.docs)} onClick={null} optionClick={null} />
  );
  return (
    <React.Fragment>
      {showDataElement}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default ArtistsScreen;
