import categories from "./categories";
import products from "./products";
import gallery from "./gallery";
import sliders from "./sliders";
import owners from "./owners";
import store from "./store";
import ownersSearch from "./ownersSearch";
import storeSearch from "./storeSearch";
import banners from "./banners";
import notifications from "./notifications";
import versions from "./versions";

const get = {
  categories,
  products,
  gallery,
  sliders,
  owners,
  store,
  ownersSearch,
  storeSearch,
  banners,
  notifications,
  versions,
};
export default get;
