import React, { useEffect, useState, Fragment, useRef } from "react";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
import utils from "../../../utils";
import Table from "../../../component/UI/Tables/Table";
import PaginationM from "../../../component/UI/PaginationM";

const NotificationScreen = (props) => {
  const { onDataChange, filters, acceptedCardInfo, requestData, handelPage } = props;

  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });
  const inputRef = useRef(null);

  const card = panelAdmin.utils.consts.card;

  const showDataElement = <ShowCardInformation data={card.notification(requestData?.docs)} onClick={null} optionClick={null} />;
  // const showDataElement = <Table tableStructure={utils.json.table.Notification} data={requestData} />;
  return (
    <React.Fragment>
      <div className="flexColum">
        {showDataElement}

        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={handelPage} />
      </div>{" "}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
    </React.Fragment>
  );
};

export default NotificationScreen;
