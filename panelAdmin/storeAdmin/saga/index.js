import { takeEvery } from "redux-saga/effects";
import atSaga from "../actionTypes/saga";
import webServices from "./webServices";

// ======================================================================================= UPLOAD
export function* watchUploadImage() {
  yield takeEvery(atSaga.UPLOAD_IMAGE_DATA, webServices.POST.uploadImage);
}
// ======================================================================================= GALLERY
export function* watchGallery() {
  yield takeEvery(atSaga.GET_GALLERY_DATA, webServices.GET.galleryData);
}
// ======================================================================================= CATEGORY
// export function* watchCategory() {
//   yield all([
//     takeEvery(atSaga.GET_CATEGORY_DATA, webServices.GET.CategoryData),
//     takeEvery(atSaga.GET_SEARCH_CATEGORY_DATA, webServices.GET.CategorySearchData),
//   ]);
// }
export function* watchGetCategoryData() {
  yield takeEvery(atSaga.GET_CATEGORY_DATA, webServices.GET.categoryData);
}
export function* watchSearchCategoryData() {
  yield takeEvery(atSaga.GET_SEARCH_CATEGORY_DATA, webServices.GET.categorySearchData);
}
// ======================================================================================= PRODUCT
export function* watchGetProductData() {
  yield takeEvery(atSaga.GET_PRODUCT_DATA, webServices.GET.productData);
}
export function* watchSearchProductData() {
  yield takeEvery(atSaga.GET_SEARCH_PRODUCT_DATA, webServices.GET.productSearchData);
}

// ======================================================================================= SLIDER
export function* watchGetSliderData() {
  yield takeEvery(atSaga.GET_SLIDER_DATA, webServices.GET.sliderData);
}
export function* watchSearchSliderData() {
  yield takeEvery(atSaga.GET_SEARCH_SLIDER_DATA, webServices.GET.sliderSearchData);
}
