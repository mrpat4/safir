import React from "react";

const song = (data) => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;

    tbody.push({ data: [title, { option: { play: true } }], style: {} });
  }
  return { thead, tbody };
};

export default song;
