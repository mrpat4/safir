import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const members = async (returnData, page, loading) => {
  return axios
    .get(Strings.ApiString.MEMBER + "/" + page)
    .then((members) => {
      console.log({ members });
      returnData(members.data);
      loading(false);
      return true;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default members;
