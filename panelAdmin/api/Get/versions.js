import panelAdmin from "../..";
import globalUtils from "../../../globalUtils";

const versions = async (page) => {
  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.VERSION;
  let getUrl = page ? strings + "/" + page : strings;

  return axios
    .get(getUrl)
    .then((res) => {
      console.log({ versions: res });

      return res;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return error;
    });
};

export default versions;
