import addCategory from "./addCategory";
import addProduct from "./addProduct";
import addGallery from "./addGallery";
import addOwner from "./addOwner";
import addStore from "./addStore";
import addSlider from "./addSlider";
import addBanner from "./addBanner";
import addNotification from "./addNotification";
import addVersion from "./addVersion";

const states = {
  addCategory,
  addProduct,
  addGallery,
  addOwner,
  addStore,
  addSlider,
  addBanner,
  addNotification,
  addVersion,
};
export default states;
