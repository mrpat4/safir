import sliderCard from "./sliderCard";
import albumCard from "./albumCard";
import topTrackCard from "./topTracksCard";
import singleCard from "./singleCard";
import comingSoonCard from "./comingSoonCard";
import moodCard from "./moodCard";
import playlistCard from "./playlistCard";
import videoCard from "./videoCard";
import PlaylistForAutuminCard from "./PlaylistForAutuminCard";
// import moreExploreCard from "./moreExploreCard";

const convert = {
  sliderCard,
  albumCard,
  singleCard,
  comingSoonCard,
  moodCard,
  playlistCard,
  videoCard,
  PlaylistForAutuminCard,
  topTrackCard,
};

export default convert;
